//
//  ProductTableViewCell.h
//  SamplePackage
//
//  Created by Suraj Singh Dheman on 03/04/17.
//  Copyright © 2017 Homepage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *behindView;
@property (weak, nonatomic) IBOutlet UIImageView *cellonImage;
@property (weak, nonatomic) IBOutlet UILabel *cellonLabel;
@end
