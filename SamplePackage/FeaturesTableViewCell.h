//
//  FeaturesTableViewCell.h
//  SamplePackage
//
//  Created by Suraj Singh Dheman on 03/04/17.
//  Copyright © 2017 Homepage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeaturesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *featureImgView;
@property (weak, nonatomic) IBOutlet UILabel *featureLbl;
@property (weak, nonatomic) IBOutlet UILabel *featureDetailLbl;
@end
