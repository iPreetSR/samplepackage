//
//  ContactUsViewController.m
//  SamplePackage
//
//  Created by Suraj Singh Dheman on 03/04/17.
//  Copyright © 2017 Homepage. All rights reserved.
//

#import "ContactUsViewController.h"

@interface ContactUsViewController ()

@end

@implementation ContactUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Setup navigation bar
    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 140, 36)];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(-10, 0, 36, 36)];
    [backButton setBackgroundImage:[UIImage imageNamed:@"backbutton.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 125, 36)];
    titleLabel.text = @"Contact Us";
    titleLabel.font = [UIFont fontWithName:@"Roboto-medium" size:16.0];
    titleLabel.textColor = [UIColor whiteColor];
    
    [rightBarButtonItems addSubview:titleLabel];
    [rightBarButtonItems addSubview:backButton];
    
    UIBarButtonItem *barButtonItem1 = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
    self.navigationItem.leftBarButtonItem = barButtonItem1;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)callButtonOnePressed:(id)sender {
    NSString *makeToast = [NSString stringWithFormat:@"tel:+919833954332"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:makeToast] options:@{} completionHandler:nil];
}

- (IBAction)callButtonSecondPressed:(id)sender {
    NSString *makeToast = [NSString stringWithFormat:@"tel:+917977889806"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:makeToast] options:@{} completionHandler:nil];
}

- (IBAction)emailButtonPressed:(id)sender {
    NSString *makeToast = [NSString stringWithFormat:@"info@BulkSMSkey.com"];
    
    NSString *emailTitle = @"Get in Touch";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:makeToast];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}

- (IBAction)closeButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult: (MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
