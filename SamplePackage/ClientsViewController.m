//
//  ClientsViewController.m
//  SamplePackage
//
//  Created by Suraj Singh Dheman on 03/04/17.
//  Copyright © 2017 Homepage. All rights reserved.
//

#import "ClientsViewController.h"
#import "DeviceTypes.h"

@interface ClientsViewController ()

@end

@implementation ClientsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Setup navigation bar
    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 140, 36)];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(-10, 0, 36, 36)];
    [backButton setBackgroundImage:[UIImage imageNamed:@"backbutton.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 125, 36)];
    titleLabel.text = @"Our Clients";
    titleLabel.font = [UIFont fontWithName:@"Roboto-medium" size:16.0];
    titleLabel.textColor = [UIColor whiteColor];
    
    [rightBarButtonItems addSubview:titleLabel];
    [rightBarButtonItems addSubview:backButton];
    
    UIBarButtonItem *barButtonItem1 = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
    self.navigationItem.leftBarButtonItem = barButtonItem1;
    
    
    //Collectionview methods
    self.clientsCollectionView.delegate = self;
    self.clientsCollectionView.dataSource = self;
    [self.clientsCollectionView registerNib:[UINib nibWithNibName:@"ClientsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 9;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([DeviceTypes isIPhone5SizedDevice]) {
        return CGSizeMake(80, 80);
    } else if ([DeviceTypes isIPhone6SizedDevice]) {
        return CGSizeMake(100, 100);
    } else if ([DeviceTypes isIPhone6PlusSizedDevice]) {
        return CGSizeMake(120, 120);
    } else {
        return CGSizeMake(100, 100);
    }
    
}

@end
