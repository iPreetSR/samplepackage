//
//  ProductDetailsViewController.m
//  SamplePackage
//
//  Created by Suraj Singh Dheman on 03/04/17.
//  Copyright © 2017 Homepage. All rights reserved.
//

#import "ProductDetailsViewController.h"

@interface ProductDetailsViewController ()

@end

@implementation ProductDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Setup navigation bar
    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 140, 36)];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(-10, 0, 36, 36)];
    [backButton setBackgroundImage:[UIImage imageNamed:@"backbutton.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 125, 36)];
    titleLabel.text = @"Product Details";
    titleLabel.font = [UIFont fontWithName:@"Roboto-medium" size:16.0];
    titleLabel.textColor = [UIColor whiteColor];
    
    [rightBarButtonItems addSubview:titleLabel];
    [rightBarButtonItems addSubview:backButton];
    
    UIBarButtonItem *barButtonItem1 = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
    self.navigationItem.leftBarButtonItem = barButtonItem1;
    
    
    //Set the objects
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *textToPut = [defaults objectForKey:@"section"];
    self.detailTextview.text = textToPut;
    
    NSString *nameofImage = [defaults objectForKey:@"nameofImage"];
    self.detailImageView.image = [UIImage imageNamed:nameofImage];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
