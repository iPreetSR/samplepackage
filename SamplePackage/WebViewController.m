//
//  WebViewController.m
//  SamplePackage
//
//  Created by Suraj Singh Dheman on 07/04/17.
//  Copyright © 2017 Homepage. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Demo App";
    
    [self.webView setDelegate:self];
    
    NSString *urlAddress = [NSString stringWithFormat:@"http://54.169.61.59/ColgateFinalComposed.pdf"];
    NSURL *url1 = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url1];
    [self.webView loadRequest:requestObj];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
