//
//  ProductViewController.m
//  SamplePackage
//
//  Created by Suraj Singh Dheman on 03/04/17.
//  Copyright © 2017 Homepage. All rights reserved.
//

#import "ProductViewController.h"
#import "ProductTableViewCell.h"

@interface ProductViewController () {
    NSArray *detailsArray;
    NSArray *detailSecArray;
    NSArray *detailsImages;
}

@end

@implementation ProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Setup navigation bar
    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 140, 36)];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(-10, 0, 36, 36)];
    [backButton setBackgroundImage:[UIImage imageNamed:@"backbutton.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 125, 36)];
    titleLabel.text = @"Products";
    titleLabel.font = [UIFont fontWithName:@"Roboto-medium" size:16.0];
    titleLabel.textColor = [UIColor whiteColor];
    
    [rightBarButtonItems addSubview:titleLabel];
    [rightBarButtonItems addSubview:backButton];
    
    UIBarButtonItem *barButtonItem1 = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
    self.navigationItem.leftBarButtonItem = barButtonItem1;
    
    
    //Tableview methods
    self.productTableView.delegate = self;
    self.productTableView.dataSource = self;
    self.productTableView.separatorColor = [UIColor clearColor];
    self.productTableView.backgroundColor = [self colorWithHexString:@"F4F4F4"];
    [self.productTableView registerNib:[UINib nibWithNibName:@"ProductTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    
    //Array initialisers
    detailsImages = [[NSArray alloc] initWithObjects:@"promotionalsms.png",@"Transactionalsms.png",@"Transactionalsms.png", nil];
    detailsArray = [[NSArray alloc] initWithObjects:@"Promotional SMS",@"Transactional SMS",@"SMS Reseller", nil];
    detailSecArray = [[NSArray alloc] initWithObjects:@"Promotional SMS is the new market mantra used to promote or sell goods and services. Considered one of the fastest, reliable and cost-effective marketing tools, marketing SMS is utilized for advertising purposes. With the help of Bulk SMS services, one can promote brands, update information.",@"As per the latest TRAI guidelines, now it is possible to send out messages through Transactional SMS to DND (Do not disturb) / NDNC registered customers. Though these messages are not of promotional nature, sometimes it becomes necessary to send out alert or service messages to clients at large.",@"Most of the Bulk SMS Resellers in India rely on us for our distinctive support and features, which have been offered till date. Every day we are improvising and enhancing the important features where any Bulk SMS Reseller can look into. Please go through the following important features of our SMS.", nil];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

# pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [detailsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ProductTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [self colorWithHexString:@"F4F4F4"];
    NSString *nameofImage = [NSString stringWithFormat:@"%@",[detailsImages objectAtIndex:indexPath.row]];
    cell.cellonImage.image = [UIImage imageNamed:nameofImage];
    cell.cellonLabel.text = [NSString stringWithFormat:@"     %@",[detailsArray objectAtIndex:indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[detailsImages objectAtIndex:indexPath.row] forKey:@"nameofImage"];
    [defaults setObject:[detailSecArray objectAtIndex:indexPath.row] forKey:@"section"];
    [defaults synchronize];
    [self performSegueWithIdentifier:@"goToDetails" sender:nil];
}

# pragma mark - UITableViewDelegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 230;
}

//hex color method
-(UIColor*)colorWithHexString:(NSString*)hex {
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

@end
