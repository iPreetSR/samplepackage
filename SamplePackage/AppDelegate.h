//
//  AppDelegate.h
//  SamplePackage
//
//  Created by Suraj Singh Dheman on 03/04/17.
//  Copyright © 2017 Homepage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

