//
//  ContactUsViewController.h
//  SamplePackage
//
//  Created by Suraj Singh Dheman on 03/04/17.
//  Copyright © 2017 Homepage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ContactUsViewController : UIViewController <MFMailComposeViewControllerDelegate>
- (IBAction)callButtonOnePressed:(id)sender;
- (IBAction)callButtonSecondPressed:(id)sender;
- (IBAction)emailButtonPressed:(id)sender;
- (IBAction)closeButton:(id)sender;

@end
