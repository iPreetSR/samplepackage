//
//  ViewController.m
//  SamplePackage
//
//  Created by Suraj Singh Dheman on 03/04/17.
//  Copyright © 2017 Homepage. All rights reserved.
//

#import "ViewController.h"
#import "LJAutoScrollView.h"
#import "DeviceTypes.h"
#import "ContactUsViewController.h"

@interface ViewController () <LJAutoScrollViewDelegate>
@property (nonatomic, strong) LJAutoScrollView *autoScrollView;
@end

static const CGFloat kAutoScrollViewHeight = 200;
static const CGFloat kAutoScrollViewHeightOnly = 250;
static const CGFloat kAutoScrollViewHeightBigOnly = 300;

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Setup the navigation bar
    [self.navigationController.navigationBar setBarTintColor:[self colorWithHexString:@"003A6B"]];
    
    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 140, 36)];
    UIImageView *backButton = [[UIImageView alloc] initWithFrame:CGRectMake(-10, 0, 80, 36)];
    backButton.image = [UIImage imageNamed:@"bulklogo-white.png"];
    [rightBarButtonItems addSubview:backButton];
    
    UIBarButtonItem *barButtonItem1 = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
    self.navigationItem.leftBarButtonItem = barButtonItem1;
    
    UIView *leftBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 40)];
    UIButton *backButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton1.frame = CGRectMake(90, 0, 35, 35);
    [backButton1 setBackgroundImage:[UIImage imageNamed:@"employer.png"] forState:UIControlStateNormal];
    [backButton1 addTarget:self action:@selector(contactPressed) forControlEvents:UIControlEventTouchUpInside];
    [leftBarButtonItems addSubview:backButton1];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftBarButtonItems];
    self.navigationItem.rightBarButtonItem = barButtonItem;
    
    //Bannerview
    if ([DeviceTypes isIPhone5SizedDevice]) {
        NSLog(@"Device is 5/5s/SE");
        self.autoScrollView = [[LJAutoScrollView alloc] initWithFrame:CGRectMake(0, 57, self.view.frame.size.width, kAutoScrollViewHeight)];
    } else if ([DeviceTypes isIPhone6SizedDevice]) {
        NSLog(@"Device is 6/6S/7");
        self.autoScrollView = [[LJAutoScrollView alloc] initWithFrame:CGRectMake(0, 57, self.view.frame.size.width, kAutoScrollViewHeightOnly)];
    } else if ([DeviceTypes isIPhone6PlusSizedDevice]) {
        NSLog(@"Device is 6+/6S+/7+");
        self.autoScrollView = [[LJAutoScrollView alloc] initWithFrame:CGRectMake(0, 57, self.view.frame.size.width, kAutoScrollViewHeightBigOnly)];
    }
    self.autoScrollView.delegate = self;
    self.autoScrollView.itemSize = CGSizeMake(self.view.frame.size.width, kAutoScrollViewHeight);
    self.autoScrollView.scrollInterval = 1000.0f;
    [self.view addSubview:self.autoScrollView];
    
    
    bannerImages = [[NSArray alloc] initWithObjects:@"banner1.png",@"banner2.png",@"banner3.png",@"banner4.png", nil];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)contactPressed {
    UIViewController *signInViewController = [self.storyboard   instantiateViewControllerWithIdentifier:@"ContactUsViewController"];
    signInViewController.providesPresentationContextTransitionStyle = YES;
    signInViewController.definesPresentationContext = YES;
    [signInViewController setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self.navigationController presentViewController:signInViewController animated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.autoScrollView startAutoScroll];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.autoScrollView stopAutoScroll];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView *)autoScrollView:(LJAutoScrollView *)autoScrollView customViewForIndex:(NSInteger)index {
    UIImageView *imageView = [[UIImageView alloc] init];
    if ([DeviceTypes isIPhone5SizedDevice]) {
        NSLog(@"Device is 5/5s/SE");
        imageView.frame = CGRectMake(0, 0, self.view.frame.size.width, kAutoScrollViewHeight);
    } else if ([DeviceTypes isIPhone6SizedDevice]) {
        NSLog(@"Device is 6/6S/7");
        imageView.frame = CGRectMake(0, 0, self.view.frame.size.width, kAutoScrollViewHeightOnly);
    } else if ([DeviceTypes isIPhone6PlusSizedDevice]) {
        NSLog(@"Device is 6+/6S+/7+");
        imageView.frame = CGRectMake(0, 0, self.view.frame.size.width, kAutoScrollViewHeightBigOnly);
    }
    [imageView setContentMode:UIViewContentModeScaleAspectFill];
    NSString *imageName = [NSString stringWithFormat:@"%@", [bannerImages objectAtIndex:index]];
    [imageView setImage:[UIImage imageNamed:imageName]];
    return imageView;
    
}

- (NSInteger)numberOfPagesInAutoScrollView:(LJAutoScrollView *)autoScrollView {
    return [bannerImages count];
}

- (void)autoScrollView:(LJAutoScrollView *)autoScrollView didSelectItemAtIndex:(NSInteger)index {
    NSLog(@"select item at index:%@", [bannerImages objectAtIndex:index]);
}

//hex color method
-(UIColor*)colorWithHexString:(NSString*)hex {
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

- (IBAction)productsPressed:(id)sender {
    [self performSegueWithIdentifier:@"goToProducts" sender:nil];
}

- (IBAction)aboutPressed:(id)sender {
    [self performSegueWithIdentifier:@"goToAbout" sender:nil];
}

- (IBAction)clientpressed:(id)sender {
    [self performSegueWithIdentifier:@"goToClients" sender:nil];
}

- (IBAction)featuresPressed:(id)sender {
    [self performSegueWithIdentifier:@"goToFeatures" sender:nil];
}



@end
