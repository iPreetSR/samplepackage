//
//  ClientsCollectionViewCell.h
//  SamplePackage
//
//  Created by Suraj Singh Dheman on 03/04/17.
//  Copyright © 2017 Homepage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClientsCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *clientImageview;

@end
