//
//  ProductTableViewCell.m
//  SamplePackage
//
//  Created by Suraj Singh Dheman on 03/04/17.
//  Copyright © 2017 Homepage. All rights reserved.
//

#import "ProductTableViewCell.h"

static int shadowOffsetWidth = 0;
static int shadowOffsetHeight = 2;
static float shadowOpacity = 0.5;

@implementation ProductTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.behindView.layer.cornerRadius = 0;
    self.behindView.layer.masksToBounds = false;
    self.behindView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.behindView.layer.shadowOffset = CGSizeMake(shadowOffsetWidth, shadowOffsetHeight);
    self.behindView.layer.shadowOpacity = shadowOpacity;
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
