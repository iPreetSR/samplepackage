//
//  ViewController.h
//  SamplePackage
//
//  Created by Suraj Singh Dheman on 03/04/17.
//  Copyright © 2017 Homepage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    NSArray *bannerImages;
}

- (IBAction)productsPressed:(id)sender;
- (IBAction)aboutPressed:(id)sender;
- (IBAction)clientpressed:(id)sender;
- (IBAction)featuresPressed:(id)sender;

@end

