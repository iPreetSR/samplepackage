//
//  FeaturesViewController.m
//  SamplePackage
//
//  Created by Suraj Singh Dheman on 03/04/17.
//  Copyright © 2017 Homepage. All rights reserved.
//

#import "FeaturesViewController.h"
#import "FeaturesTableViewCell.h"

@interface FeaturesViewController () {
    NSArray *featureList;
    NSArray *featureListAraay;
}

@end

@implementation FeaturesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Setup navigation bar
    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 140, 36)];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(-10, 0, 36, 36)];
    [backButton setBackgroundImage:[UIImage imageNamed:@"backbutton.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 125, 36)];
    titleLabel.text = @"Features";
    titleLabel.font = [UIFont fontWithName:@"Roboto-medium" size:16.0];
    titleLabel.textColor = [UIColor whiteColor];
    
    [rightBarButtonItems addSubview:titleLabel];
    [rightBarButtonItems addSubview:backButton];
    
    UIBarButtonItem *barButtonItem1 = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
    self.navigationItem.leftBarButtonItem = barButtonItem1;
    
    
    //Tableview methods
    self.featureTableView.delegate = self;
    self.featureTableView.dataSource = self;
    self.featureTableView.separatorColor = [UIColor clearColor];
    self.featureTableView.backgroundColor = [self colorWithHexString:@"F4F4F4"];
    [self.featureTableView registerNib:[UINib nibWithNibName:@"FeaturesTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    
    
    //array initialisers
    featureList = [[NSArray alloc] initWithObjects:@"Easy To Use",@"Instant Delivery",@"Unlimited Validity",@"Quick Report",@"Developer API",@"24/7 Support", nil];
    featureListAraay = [[NSArray alloc] initWithObjects:@"One place to take charge of your content with a learning curve that's a breeze! No set-up. No installation. Works right in your browser. It's Just that Easy.",@"Bulk SMS Key provides lightning fast delivery for both transactional and promotional SMS. All SMS can be tracked by its delivery time.",@"No need to worry about validity of SMS. Bulk SMS Key provides unlimited validity for both promotional and transactional SMS.",@"Keep a track of anything and everything. You can track sent SMS logs, delivery time, failed numbers and more.",@"SMS API allows you to automate the sending and receiving of SMS messages by integrating our SMS gateway directly with your business applications, websites or other software.",@"Our Team provides around the clock customer support for all issues. Customer satisfaction is important to us.", nil];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

# pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [featureList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FeaturesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [self colorWithHexString:@"F4F4F4"];
    cell.featureLbl.text = [NSString stringWithFormat:@"%@",[featureList objectAtIndex:indexPath.row]];
    cell.featureDetailLbl.text = [NSString stringWithFormat:@"%@",[featureListAraay objectAtIndex:indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

# pragma mark - UITableViewDelegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 164;
}

//hex color method
-(UIColor*)colorWithHexString:(NSString*)hex {
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

@end
