//
//  ClientsViewController.h
//  SamplePackage
//
//  Created by Suraj Singh Dheman on 03/04/17.
//  Copyright © 2017 Homepage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClientsViewController : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *upTopLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *clientsCollectionView;

@end
